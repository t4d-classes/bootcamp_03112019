
// setTimeout(() => {
//   console.log('test')
// }, 2000);

// console.log('hello');

// setTimeout(() => {
//     console.log('a');
//     setTimeout(() => {
//         console.log('b');
//         setTimeout(() => {
//             console.log('c');
//         }, 500);
//     }, 1000);
// }, 2000);

// const con = new Connection(connectionString);
// con.open();

// const stmt = con.prepareStatement(sql);

// const rst = stmt.query(param1, param2);

// console.log(rst);

// con.open(function(con) {
//   con.prepareStatement(sql, function(stmt) {
//     stmt.query(param1, param2, function(rst) {

//     })
//   })
// })


// console.log('made it here');

const allDone = () => {
  console.log('all done');
};

let counter = 0;

counter++;
setTimeout(() => {
  console.log('a');
  counter--;
  if (counter === 0) {
    allDone();
  }
}, 2000);

counter++;
setTimeout(() => {
  console.log('b');
  counter--;
  if (counter === 0) {
    allDone();
  }
}, 500);

counter++;
setTimeout(() => {
  console.log('c');
  counter--;
  if (counter === 0) {
    allDone();
  }
}, 1000);

// Exercise 
// All of the setTimeout calls should occur in the same task, when all three callbacks have executed, run the allDone function. Do not wrap allDone in a setTimeout of 3000... You know what I mean...
