import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { createStore, bindActionCreators, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';

const calcReducer = (state = { result: 0 }, action) => {
  console.log('state:', state, 'action:', action);

  switch (action.type) {
    case 'ADD':
      return {
        ...state,
        result: state.result + action.value,
      };
    case 'SUBTRACT':
    return {
      ...state,
      result: state.result - action.value,
    };
    default:
      return state;
  }
};

// const createStore = (reducerFn) => {

//   let currentState = undefined;
//   const subscriberFns = [];

//   return {
//     getState: () => currentState,
//     dispatch: (action) => {
//       currentState = reducerFn(currentState, action);
//       subscriberFns.forEach(callbackFn => callbackFn());
//     },
//     subscribe: (callbackFn) => {
//       subscriberFns.push(callbackFn);
//     },
//   };

// };

const calcStore = createStore(calcReducer, composeWithDevTools());



// const bindActionCreators = (actions, dispatch) => {
//   return Object.keys(actions).reduce( (boundActions, actionKey) => {
//     boundActions[actionKey] = (...params) => dispatch(actions[actionKey](...params));
//     return boundActions;
//   }, {});
// };

const CalcTool = ({ result, onAdd, onSubtract, onMultiply, onDivide }) => {

  const [ numInput, setNumInput ] = useState(0);

  return <div>
    Result: {result}<br />
    <input type="number" value={numInput} onChange={e => setNumInput(Number(e.target.value))} />
    <button type="button" onClick={() => onAdd(numInput)}>+</button>
    <button type="button" onClick={() => store.dispatch(createAddAction(numInput))}>-</button>
    <button type="button" onClick={() => onMultiply(numInput)}>*</button>
    <button type="button" onClick={() => onDivide(numInput)}>/</button>
  </div>;

};

// const connect = (mapStateToPropsFn, mapDispatchToPropsFn) => {

//   return PresentationalComponent => {

//     return class ContainerComponent extends React.Component {
//       constructor(props) {
//         super(props);

//         this.dispatchProps = mapDispatchToPropsFn(props.store.dispatch);
//       }

//       // init
//       componentDidMount() {

//         this.unsubscribe = this.props.store.subscribe(() => {
//           this.forceUpdate();
//         });

//       }

//       // destroy
//       componentWillUnmount() {
//         // this.unsubscribe();
//       }

//       render() {
//         const stateProps = mapStateToPropsFn(this.props.store.getState());
//         return <PresentationalComponent {...this.dispatchProps} {...stateProps} />;
//       }
//     };
//   };
// };

const createCalcToolContainer = connect(

  // map state to props -> setting up the data to be passed to CalcTool
  ({ result }) => ({ result }),
  // map dispatch to props -> setting up the functions to be passed to CalcTool
  dispatch => bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
    onMultiply: createMultiplyAction,
    onDivide: createDivideAction,
  }, dispatch),
);

const CalcToolContainer = createCalcToolContainer(CalcTool);


ReactDOM.render(
  <Provider store={calcStore}>
    <CalcToolContainer />
  </Provider>,
  document.querySelector('#root'),  
);





