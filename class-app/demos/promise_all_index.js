
// const p = new Promise(resolve => {
//   setTimeout(() => {
//     resolve('a');
//   }, 2000)
// });


const p1 = new Promise(resolve => setTimeout(() => resolve('a'), 2000));
const p2 = new Promise(resolve => setTimeout(() => resolve('b'), 4000));
const p3 = new Promise( (resolve, reject) => setTimeout(() => reject('c'), 6000));
const p4 = new Promise(resolve => setTimeout(() => resolve('d'), 8000));

p1.then(result => console.log(result));
p2.then(result => console.log(result));
p3.then(result => console.log(result)).catch(result => console.log('rejected: ', result));
p4.then(result => console.log(result));

Promise.all([ p1, p2, p3, p4 ]).then(results => {
  console.log('all done');
  console.log(results);
}).catch(result => {
  console.log('one failed');
  console.log(result);
});




// fetch('http://localhost:3050/cars')
//   .then(res => res.json())
//   .then(cars => console.log(cars));
  
// fetch('http://localhost:3050/cars', {
//   method: 'POST',
//   headers: { 'Content-Type': 'application/json' },
//   body: JSON.stringify({ make: 'a', model: 'r', year: 1254, color: 'g', price: 2 }),
// })
//   .then(res => res.json())
//   .then(car => console.log(car));