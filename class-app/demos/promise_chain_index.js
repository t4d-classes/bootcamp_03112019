
// class Promise {
//   constructor(asyncOpFn) {

//     this.thenFns = [];
//     this.catchFns = [];

//     const resolve = (data) => {
//       this.thenFns.forEach(thenFn => {
//         thenFn(data);
//       });
//     };

//     const reject = (data) => {
//       this.catchFns.forEach(catchFn => {
//         catchFn(data);
//       });
//     };

//     asyncOpFn(resolve, reject);
//   }

//   then(callback) {
//     this.thenFns.push(callback);
//   }

//   catch(callback) {
//     this.catchFns.push(callback);
//   }
// }


const doProposal = () => {
  return new Promise(function youngLady(resolve, reject) {
    setTimeout(() => {
      console.log('she says yes');
      resolve('she is madly in love...');
    }, 2000);
  });
};

const findLocation = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('found a location');
    }, 2000);
  });  
}

const getClothing = () => {
  return new Promise(function goFindClothing(resolve) {
    setTimeout(() => {
      resolve('found wedding clothes');
    }, 2000);
  });
};


const tieTheKnot = async () => {

  try {

  const proposalResult = await doProposal();
  console.log(proposalResult);

  const locationResult = await findLocation();
  console.log(locationResult);

  const clothingResult = await getClothing();
  console.log(clothingResult);

  } catch (err) {
    console.log('the world ended');
  }

};

tieTheKnot()
  .then(() => console.log('all done'))
  .catch(() => console.log('no more sunshine and summertime'));




// const youngMan = new Promise(function youngLady(resolve, reject) {

//   setTimeout(() => {
//     console.log('she says yes');
//     resolve('she is madly in love...');
//   }, 2000);

// });

// youngMan.then((result) => {
//   console.log('yay! she said yes!');
//   console.log(result);

//   console.log('find wedding clothes');
//   return new Promise(function goFindClothing(resolve) {
//     setTimeout(() => {
//       resolve('found wedding clothes');
//     }, 2000);
//   });

// }).catch(() => {

//   return Promise.reject('some value');

// }).then((result) => {

//   console.log(result);
//   console.log('find a location');
//   return new Promise(resolve => {
//     setTimeout(() => {
//       resolve('found a location');
//     }, 2000);
//   });  

// }).then(result => {

//   console.log(result);
//   console.log('all ready to tie the knot...');

// }).catch((result) => {
//   console.log('she said no, time to sign up for eHarmony...');
//   console.log(result);
// });

// console.log('young man is waiting...');


function* nums() {

  console.log('yielding 1');
  yield 1;

  console.log('yielding 2');
  yield 2;

  console.log('yielding 3');
  yield 3;
}

for (const num of nums()) {
  console.log('for iteration');
  console.log(num);
}
