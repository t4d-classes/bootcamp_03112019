
const xhr = new XMLHttpRequest();

xhr.addEventListener('readystatechange', () => {

  if (xhr.status >= 200 && xhr.status < 400 && xhr.readyState === 4) {
    console.log(JSON.parse(xhr.responseText));
  }

});

xhr.open('POST', 'http://localhost:3050/cars');

// no request body, pass no arguments
xhr.send();

// have a request body, pass arguments
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.send(JSON.stringify({
  make: 't', model: 'r', year: 1234, color: 'pink', price: 10
}));

// Exercise

// Create a function which has the following name (myFetch) and function signature
// the following should be able to work if pasted into your file

myFetch('http://localhost:3050/cars').then(cars => console.log(cars));

// upgrade myFetch to work with the following function call
myFetch('http://localhost:3050/cars', {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    make: 't', model: 'r', year: 1234, color: 'pink', price: 10
  })
}).then(insertedCar => console.log(insertedCar));