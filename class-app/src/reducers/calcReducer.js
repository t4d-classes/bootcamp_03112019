import { ADD, SUBTRACT, MULTIPLY, DIVIDE } from '../actions/calcActions';

export const calcReducer = (state = { result: 0 }, action) => {
  switch (action.type) {
    case ADD:
      return {
        ...state,
        result: state.result + action.value,
      };
    case SUBTRACT:
      return {
        ...state,
        result: state.result - action.value,
      };
    case MULTIPLY:
      return {
        ...state,
        result: state.result * action.value,
      };
    case DIVIDE:
      return {
        ...state,
        result: state.result / action.value,
      };
    default:
      return state;
  }
};