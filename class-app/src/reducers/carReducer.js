import {
  APPEND_CAR, REPLACE_CAR, DELETE_CAR,
  EDIT_CAR, CANCEL_CAR, REFRESH_CARS_DONE,
} from '../actions/carActions';

// state = cars array
export const carsReducer = (state = [], action) => {
  switch (action.type) {
    case REFRESH_CARS_DONE:
      return action.payload;
    case APPEND_CAR:
      return state.concat({
          ...action.payload,
          id: Math.max(...state.map(c => c.id), 0) + 1,
        });
    case REPLACE_CAR:
      const carIndex = state.findIndex(c => c.id === action.payload.id);
      const newCars = state.concat();
      newCars[carIndex] = action.payload;
      return newCars;
    case DELETE_CAR:
      return state.filter(c => c.id !== action.payload);
    default:
      return state;
  }
};

// state = edit car id value
export const editCarIdReducer = (state = -1, action) => {

  switch (action.type) {
    case REFRESH_CARS_DONE:
    case APPEND_CAR:
    case REPLACE_CAR:
    case DELETE_CAR:
    case CANCEL_CAR:
      return -1;
    case EDIT_CAR:
      return action.payload;
    default:
      return state;
  }

};

// export const carReducer = (state = { cars: [], editCarId: -1 }, action) => {
//   switch (action.type) {
//     case REFRESH_CARS_DONE:
//       return {
//         ...state,
//         cars: action.payload,
//       };
//     case APPEND_CAR:
//       return {
//         ...state,
//         cars: state.cars.concat({
//           ...action.payload,
//           id: Math.max(...state.cars.map(c => c.id), 0) + 1,
//         }),
//         editCarId: -1,
//       };
//     case REPLACE_CAR:

//       const carIndex = state.cars.findIndex(c => c.id === action.payload.id);
//       const newCars = state.cars.concat();
//       newCars[carIndex] = action.payload;

//       return {
//         ...state,
//         cars: newCars,
//         editCarId: -1,
//       };
//     case DELETE_CAR:
//       return {
//         ...state,
//         cars: state.cars.filter(c => c.id !== action.payload),
//         editCarId: -1,
//       };
//     case EDIT_CAR:
//       return {
//         ...state,
//         editCarId: action.payload,
//       };
//     case CANCEL_CAR:
//       return {
//         ...state,
//         editCarId: -1,
//       };
//     default:
//       return state;
//   }
// };