import React from 'react';
import styled from 'styled-components';

// import './ToolHeader.css';

const Title = styled.h1.attrs({
  'data-test': 'test',
})`
  font-weight: ${ ({ isBold }) => isBold ? 'bold' : 'normal'};
  color: purple;
`;

// const RedTitle = styled(Title)`
//   color: red;
// `;

// const BlueTitle = styled(Title)`
//   color: blue;
// `;

// const _ToolHeader = ({ headerText, className }) => {
//   return <header>
//     <h1 className={className}>{headerText}</h1>
//   </header>;
// };

// export const ToolHeader = styled(_ToolHeader)`
//   font-weight: ${ ({ isBold }) => isBold ? 'bold' : 'normal'}; 
//   color: green;
// `;

export const ToolHeader = ({ headerText }) => {
  return <Title>{headerText}</Title>;
}

/* export const ToolHeader = ({ headerText, isBold }) => {
  return <header className="ToolHeader">
    <RedTitle isBold={isBold}>{headerText}</RedTitle>
    <BlueTitle isBold={isBold}>{headerText}</BlueTitle>
  </header>;
}; */