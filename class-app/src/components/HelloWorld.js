import React from 'react';

export const HelloWorld = () => {

  // return React.createElement('header', null, 
  //   React.createElement('h1', null, 'Hello World!'),
  //   React.createElement('h2', null, 'Subheader'),
  // );

  return (
    <header>
      <h1>Hello World!</h1>
      <h2>Subheader</h2>
    </header>
  );
};