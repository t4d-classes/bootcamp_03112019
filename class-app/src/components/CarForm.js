import React, { useState } from 'react';

export const CarForm = ({ buttonText, onSubmitCar }) => {

  const getInitialCarForm = () => ({
    make: '',
    model: '',
    year: 1900,
    color: '',
    price: 0,
  });

  const [ carForm, setCarForm ] = useState(getInitialCarForm());

  const change = ({ target: { name, type, value } }) => {
    setCarForm({
      ...carForm,
      [ name ]: type === 'number' ? Number(value) : value,
    });
  };

  const submitCar = () => {
    onSubmitCar({ ...carForm });
    setCarForm(getInitialCarForm());
  };

  return <>
    <form>
      <div>
        <label htmlFor="make-input">Make:</label>
        <input type="text" id="make-input" value={carForm.make} name="make" onChange={change} />
      </div>
      <div>
        <label htmlFor="model-input">Model:</label>
        <input type="text" id="model-input" value={carForm.model} name="model" onChange={change} />
      </div>
      <div>
        <label htmlFor="year-input">Year:</label>
        <input type="number" id="year-input" value={carForm.year} name="year" onChange={change} />
      </div>
      <div>
        <label htmlFor="color-input">Color:</label>
        <input type="text" id="color-input" value={carForm.color} name="color" onChange={change} />
      </div>
      <div>
        <label htmlFor="price-input">Price:</label>
        <input type="number" id="price-input" value={carForm.price} name="price" onChange={change} />
      </div>
      <button type="button" onClick={submitCar}>{buttonText}</button>
    </form>
  </>;

};