import React, { useEffect } from 'react';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export const CarTool = ({
  cars, editCarId,
  onAppendCar: appendCar, onReplaceCar: replaceCar,
  onDeleteCar: deleteCar, onEditCar: editCar,
  onCancelCar: cancelCar, onRefreshCars: refreshCars,
}) => {

  // run the code when the component loads
  useEffect(() => {
    setTimeout(() => refreshCars(), 3000);
  }, []);

  return <>
    <ToolHeader headerText="Car Tool" />
    <CarTable cars={cars} editCarId={editCarId}
      onEditCar={editCar} onDeleteCar={deleteCar}
      onSaveCar={replaceCar} onCancelCar={cancelCar} />
    <CarForm buttonText="Add Car" onSubmitCar={appendCar} /> 
  </>;

};