import React, { useState } from 'react';

export const CalcTool = ({ result, onAdd, onSubtract, onMultiply, onDivide }) => {

  const [ numInput, setNumInput ] = useState(0);

  return <div>
    Result: {result}<br />
    <input type="number" value={numInput} onChange={e => setNumInput(Number(e.target.value))} />
    <button type="button" onClick={() => onAdd(numInput)}>+</button>
    <button type="button" onClick={() => onSubtract(numInput)}>-</button>
    <button type="button" onClick={() => onMultiply(numInput)}>*</button>
    <button type="button" onClick={() => onDivide(numInput)}>/</button>
  </div>;

};