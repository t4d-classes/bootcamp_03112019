import React, { useState } from 'react';

export const ColorForm = (props) => {

  const [ colorForm, setColorForm ] = useState({
    color: '',
  });

  const change = e => {
    setColorForm({
      ...colorForm,
      [ e.target.name ]: e.target.type === 'number' ? Number(e.target.value) : e.target.value,
    });
  };

  const submitColor = () => {
    props.onSubmitColor(colorForm.color);

    setColorForm({
      ...colorForm,
      color: '',
    });
  };

  return <form>
    <div>
      <label htmlFor="color-input">Color:</label>
      <input type="text" id="color-input" value={colorForm.color} name="color" onChange={change} />
    </div>
    <button type="button" onClick={submitColor}>{props.buttonText}</button>
  </form>;

};