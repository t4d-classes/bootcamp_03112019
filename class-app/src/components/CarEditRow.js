import React, { useState } from 'react';

export const CarEditRow = ({ car, onSaveCar, onCancelCar }) => {

  const [ carForm, setCarForm ] = useState(car);

  const change = ({ target: { name, type, value } }) => {
    setCarForm({
      ...carForm,
      [ name ]: type === 'number' ? Number(value) : value,
    });
  };

  console.log(carForm);

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" id="edit-make-input" value={carForm.make} name="make" onChange={change} /></td>
    <td><input type="text" id="edit-model-input" value={carForm.model} name="model" onChange={change} /></td>
    <td><input type="number" id="edit-year-input" value={carForm.year} name="year" onChange={change} /></td>
    <td><input type="text" id="edit-color-input" value={carForm.color} name="color" onChange={change} /></td>
    <td><input type="number" id="edit-price-input" value={carForm.price} name="price" onChange={change} /></td>
    <td>
      <button type="button" onClick={() => onSaveCar({ ...carForm }) }>Save</button>
      <button type="button" onClick={onCancelCar}>Cancel</button>
    </td>
  </tr>;

};