import React, { useState } from 'react';

import { ToolHeader } from './ToolHeader';
import { ColorForm } from './ColorForm';

export const ColorTool = (props) => {

  const [ colors, setColors ] = useState(props.colors.concat());

  const addColor = (newColor) => {
    setColors(colors.concat(newColor));
  };

  return <>
    <ToolHeader headerText="Color Tool" isBold />
    <ul>
      {colors.map(color => <li key={color}>{color}</li>)}
    </ul>
    <ColorForm onSubmitColor={addColor} buttonText="Add" />
  </>;

}