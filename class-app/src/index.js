import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// import { calcStore } from './stores/calcStore';
// import { CalcToolContainer } from './containers/CalcToolContainer';
//
// ReactDOM.render(
//   <Provider store={calcStore}>
//     <CalcToolContainer />
//   </Provider>,
//   document.querySelector('#root'),
// );

import { carStore } from './stores/carStore';
import { CarToolContainer } from './containers/CarToolContainer';

ReactDOM.render(
  <Provider store={carStore}>
    <>
      <CarToolContainer />
      <CarToolContainer />
    </>
  </Provider>,
  document.querySelector('#root'),
);





