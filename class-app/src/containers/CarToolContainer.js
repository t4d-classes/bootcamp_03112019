import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  createAppendCarAction, createReplaceCarAction,
  createDeleteCarAction, createEditCarAction,
  createCancelCarAction, refreshCars
} from '../actions/carActions';
import { CarTool } from '../components/CarTool';

const createCarToolContainer = connect(

  // map state to props -> setting up the data to be passed to CalcTool
  ({ cars, editCarId }) => ({ cars, editCarId }),
  // map dispatch to props -> setting up the functions to be passed to CalcTool
  dispatch => bindActionCreators({
    onRefreshCars: refreshCars,
    onAppendCar: createAppendCarAction,
    onReplaceCar: createReplaceCarAction,
    onDeleteCar: createDeleteCarAction,
    onEditCar: createEditCarAction,
    onCancelCar: createCancelCarAction,
  }, dispatch),
);

export const CarToolContainer = createCarToolContainer(CarTool);