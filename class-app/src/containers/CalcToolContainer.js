import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  createAddAction, createSubtractAction,
  createMultiplyAction, createDivideAction
} from '../actions/calcActions';
import { CalcTool } from '../components/CalcTool';

const createCalcToolContainer = connect(

  // map state to props -> setting up the data to be passed to CalcTool
  ({ result }) => ({ result }),
  // map dispatch to props -> setting up the functions to be passed to CalcTool
  dispatch => bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
    onMultiply: createMultiplyAction,
    onDivide: createDivideAction,
  }, dispatch),
);

export const CalcToolContainer = createCalcToolContainer(CalcTool);