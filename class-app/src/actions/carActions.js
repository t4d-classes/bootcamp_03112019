export const REFRESH_CARS_REQUEST = 'REFRESH_CARS_REQUEST';
export const REFRESH_CARS_DONE = 'REFRESH_CARS_DONE';

export const APPEND_CAR = 'APPEND_CAR';
export const REPLACE_CAR = 'REPLACE_CAR';
export const DELETE_CAR = 'DELETE_CAR';
export const EDIT_CAR = 'EDIT_CAR';
export const CANCEL_CAR = 'CANCEL_CAR';

export const createRefreshCarsRequestAction = () =>
  ({ type: REFRESH_CARS_REQUEST });
export const createRefreshCarsDoneAction = (cars) =>
  ({ type: REFRESH_CARS_DONE, payload: cars });

export const refreshCars = () => {
  return dispatch => {
    // step 1: dispatch the request action
    dispatch(createRefreshCarsRequestAction());
    // step 2: make the request to the server
    return fetch('http://localhost:3050/cars')
      .then(res => res.json())
      // step 3: dispatch the done action
      .then(cars => dispatch(createRefreshCarsDoneAction(cars)));
  };
}

export const createAppendCarAction = payload =>
  ({ type: APPEND_CAR, payload });
export const createReplaceCarAction = payload =>
  ({ type: REPLACE_CAR, payload });
export const createDeleteCarAction = payload =>
  ({ type: DELETE_CAR, payload });
export const createEditCarAction = payload =>
  ({ type: EDIT_CAR, payload });
export const createCancelCarAction = () =>
  ({ type: CANCEL_CAR });