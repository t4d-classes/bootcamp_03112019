export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const MULTIPLY = 'MULTIPLY';
export const DIVIDE = 'DIVIDE';

export const createAddAction = value => ({ type: ADD, value });
export const createSubtractAction = value => ({ type: SUBTRACT, value });
export const createMultiplyAction = value => ({ type: MULTIPLY, value });
export const createDivideAction = value => ({ type: DIVIDE, value });