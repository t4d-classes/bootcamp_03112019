import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { carsReducer, editCarIdReducer } from '../reducers/carReducer';

// const carToolReducer = combineReducers({
//   cars: carsReducer,
//   editCarId: editCarIdReducer,
// });

const carToolReducer = (state = { cars: [], editCarId: -1 }, action) => {

  const newCars = carsReducer(state.cars, action);

  const editCarId = editCarIdReducer(state.editCarId, action);

  return { car: newCars, editCarId };

}

export const carStore = createStore(
  carToolReducer,
  composeWithDevTools(applyMiddleware(thunk)),
);
