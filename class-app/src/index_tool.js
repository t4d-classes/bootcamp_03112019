import React from 'react';
import ReactDOM from 'react-dom';

// import { HelloWorld } from './components/HelloWorld';
import { ColorTool } from './components/ColorTool';
import { CarTool } from './components/CarTool';

const colorList = [ 'hot pink', 'orange', 'no color', 'yellow', 'black' ];

const carList = [
  { id: 1, make: 'Nissan', model: 'Pathfinder', year: 2016, color: 'midnight blue', price: 40000 },
  { id: 2, make: 'Nissan', model: 'Versa', year: 2014, color: 'blue', price: 17000 },
]

ReactDOM.render(
  <>
    <ColorTool colors={colorList} />
    <CarTool cars={carList} />
  </>,
  document.querySelector('#root'),
);

// ReactDOM.render(<ToolHeader headerText="1" />, document.querySelector('#root'))
// ReactDOM.render(<ToolHeader headerText="2" />, document.querySelector('#root'))