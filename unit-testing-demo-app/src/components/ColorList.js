import React from 'react';

export const ColorList = ({ colors, onDeleteColor }) => {
  return <ul>
    {colors.map(color =>
      <li key={color}>
        {color}
        <button type="button" onClick={() => onDeleteColor(color)}>Delete</button>
      </li>)}
  </ul>;
};
