# Welcome to the Front-End Boot Camp

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Friday, next 2 weeks: 9am to 5pm PST

Breaks:

- Morning: 10:25am to 10:35am
- Lunch: Noon to 1pm
- Afternoon #1: 2:15pm to 2:25pm
- Afternoon #2: 3:40pm to 3:50pm

## Course Outline

- 2 days: HTML/CSS/JS
- 2 days: React
- 2 days: Redux
- 2 days: Apollo + GraphQL
- 1.5 days: Project
- 0.5 day: Unit Testing + Other Topics

## Project Setup

### Requirements

- Node.js (version 10 or later)
- Web Browser

### Instructions

**Step 1.** Install the Parcel NPM package globally:

```sh
npm install -g parcel-bundler
```
<br>

**Step 2.** Create a new folder to hold your project files. Change into the new folder and create a new folder named 'app'<br>

**Step 3.** Create a new HTML named **index.html** in the **app** folder. Copy and paste the following content into it

```html
Hello World!
```
<br>

**Step 4.** Run the web application using the following command from the terminal window

```sh
parcel index.html
```
<br>

**Step 5.** Open your web browser and browse to **http://localhost:1234**<br>

### FAQ

1. If you have another version of Node.js installed on your system, then I recommend installing [NVM](https://github.com/creationix/nvm) (for Mac & Linux) or [NVM-Windows](https://github.com/coreybutler/nvm-windows). Both tools support the installation of multiple versions of Node.js and provide tools for easily switching between those versions. Using these tools, installing Node.js version 10 without losing your older version should be possible.

## Links

### Instructor's Resources

- [DevelopIntelligence](http://www.developintelligence.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)

### Useful Resources

- [MDN HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
- [CSS Selectors](https://www.w3schools.com/cssref/css_selectors.asp)
- [CSS Specs](https://www.w3.org/Style/CSS/specs.en.html)
- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
