export const typeDefs = `
  type Query {
    message: String
    widgets: [Widget]
    widget(widgetId: ID): Widget
    widgetsByColor(color: String): [Widget]
  }

  type Mutation {
    appendWidget(widget: AppendWidget): Widget
  }

  type Widget {
    id: ID
    name: String
    description: String
    color: String
    price: Float
    quantity: Int
  }

  input AppendWidget {
    name: String
    description: String
    color: String
    price: Float
    quantity: Int
  }

`;
