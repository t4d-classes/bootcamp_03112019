import fetch from 'node-fetch';

export const resolvers = {
  Query: {
    message: async () => {
      const res = await fetch('http://localhost:3020/apps/1');
      const app = await res.json();
      return app.message;
    },
    widgets: async (_1, _2, { restURL }) => {
      const res = await fetch(`${restURL}/widgets`);
      const widgets = await res.json();
      return widgets;
    },
    widget: async (_, { widgetId }, { restURL }) => {
      const res = await fetch(`${restURL}/widgets/${widgetId}`);
      const widget = await res.json();
      return widget;
    },
    widgetsByColor: async (_, { color }, { restURL }) => {
      const res = await fetch(`${restURL}/widgets?color=${color}`);
      const widget = await res.json();
      return widget;
    },
  },
  Mutation: {
    appendWidget: async (_, { widget }, { restURL }) => {

      const res = await fetch(`${restURL}/widgets`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(widget),
      });
      
      const appendedWidget = await res.json();

      return appendedWidget;
    },
  },
};
