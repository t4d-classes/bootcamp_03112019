import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { WidgetForm } from '../components/WidgetForm';

export const ALL_WIDGETS_QUERY = gql`
  query AllWidgets {
    widgets {
      id
      name
      description
      color
      price
      quantity
    }
  }
`;

const APPEND_WIDGET_MUTATION = gql`
  mutation AppendWidget($widget: AppendWidget) {
    appendWidget(widget: $widget) {
      id
      name
      description
      color
      price
      quantity
    }
  }
`;

export const AppendWidgetMutation = () => {

  return <Mutation mutation={APPEND_WIDGET_MUTATION}>
    {(mutateAppendWidget) => {

      const appendWidget = (widget) => {

        mutateAppendWidget({
          variables: { widget },
          update: (store, { data: { appendWidget: appendedWidget } }) => {

            const data = store.readQuery({ query: ALL_WIDGETS_QUERY });
            data.widgets = data.widgets.concat(appendedWidget);
            store.writeQuery({ query: ALL_WIDGETS_QUERY, data });
          },
        });

      };

      return <WidgetForm onSubmitWidget={appendWidget} />;

    }}
  </Mutation>;

};


