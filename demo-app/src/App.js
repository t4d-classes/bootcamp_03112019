import React from 'react';

import { AllWidgetsQuery } from './queries/AllWidgetsQuery';
import { AppendWidgetMutation } from './mutations/AppendWidgetMutation';

export const App = () => {
  return <>
    <AllWidgetsQuery />
    <AppendWidgetMutation />
  </>;
};
