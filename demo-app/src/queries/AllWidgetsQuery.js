import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import { WidgetTable } from '../components/WidgetTable';

export const ALL_WIDGETS_QUERY = gql`
  query AllWidgets {
    widgets {
      id
      name
      description
      color
      price
      quantity
    }
  }
`;

export const AllWidgetsQuery = () => {

  return <Query query={ALL_WIDGETS_QUERY}>
    {({ loading, error, data}) => {

      if (loading) return 'Loading';
      if (error) return 'Error: ' + error;

      return <WidgetTable widgets={data.widgets} />;
    }}
  </Query>;

};
