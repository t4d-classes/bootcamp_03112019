import React from 'react';

export const WidgetTable = ({ widgets }) => {

  return <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Desc</th>
        <th>Color</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
      {widgets.map(w => <tr key={w.id}>
        <td>{w.id}</td>
        <td>{w.name}</td>
        <td>{w.desc}</td>
        <td>{w.color}</td>
        <td>{w.price}</td>
        <td>{w.quantity}</td>
      </tr>)}
    </tbody>
  </table>;

};