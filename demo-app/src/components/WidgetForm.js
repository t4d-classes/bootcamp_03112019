import React from 'react';
import PropTypes from 'prop-types';

// const ToolHeader = (props) => {

// };

// ToolHeader.propTypes = {
//   buttonText: PropTypes.string,
//   onSubmitWidget: PropTypes.func.isRequired,
// };

// ToolHeader.defaultProps = {
//   buttonText: 'Submit Widget',
// };


export class WidgetForm extends React.Component {

  static propTypes = {
    buttonText: PropTypes.string,
    onSubmitWidget: PropTypes.func.isRequired,
  }

  static defaultProps = {
    buttonText: 'Submit Widget',
  };

  // class property - not valid JavaScript
  state = {
    name: '',
    description: '',
    color: '',
    price: 0.0,
    quantity: 0,
  };

  // class arrow function - this is not valid JavaScript
  change = e => {

    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value)
        : e.target.value
    });
  }; 

  submitWidget = () => {

    this.props.onSubmitWidget({ ...this.state });

    this.setState({
      name: '',
      description: '',
      color: '',
      price: 0.0,
      quantity: 0,
    });
  };


  // babel transpiles the above into this:
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     name: '',
  //     description: '',
  //     color: '',
  //     price: 0.0,
  //     quantity: 0,
  //   };

  //   this.change = this.change.bind(this);
  // }

  // change(e) {

  //   this.setState({
  //     [ e.target.name ]: e.target.type === 'number'
  //       ? Number(e.target.value)
  //       : e.target.value
  //   });
  // }


  render() {

    return <form>
      <div>
        <label>Name:</label>
        <input type="text" name="name" value={this.state.name}
          onChange={this.change} />
      </div>
      <div>
        <label>Description:</label>
        <input type="text" name="description" value={this.state.description}
          onChange={this.change} />
      </div>
      <div>
        <label>Color:</label>
        <input type="text" name="color" value={this.state.color}
          onChange={this.change} />
      </div>
      <div>
        <label>Price:</label>
        <input type="number" name="price" value={this.state.price}
          onChange={this.change} />
      </div>
      <div>
        <label>Quantity:</label>
        <input type="number" name="quantity" value={this.state.quantity}
          onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitWidget}>{this.props.buttonText}</button>
    </form>;

  }


}